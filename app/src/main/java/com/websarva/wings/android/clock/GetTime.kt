package com.websarva.wings.android.clock

import android.icu.util.Calendar
import android.util.Log

// 時間の取得、文字列の出力
class GetTime{
    private val calendar = Calendar.getInstance()

    private val hour: Int = calendar.get(Calendar.HOUR_OF_DAY)
    private val hourOfDay: Int = calendar.get(Calendar.HOUR)
    private val minute = calendar.get(Calendar.MINUTE)
    private val second = calendar.get(Calendar.SECOND)
    private val milliSecond = calendar.get(Calendar.MILLISECOND).toLong()

    // クラス内のインスタンスを引っ張ってきて文字列に加工する
    fun storeTimeInstance(fullTime: Boolean): String {
        Log.i("MyClock.GetTime", "Instance Time and returning.")

        val hourPrime = if (fullTime) "" else if (hour < 12) Application.instance.getString(R.string.AM) else Application.instance.getString(R.string.PM)
        val hourDisplay = if (fullTime) hour else hourOfDay
        val minuteStr = minute.toString().padStart(2, '0')
        val secondStr = second.toString().padStart(2, '0')
        val milliSecondStr = milliSecond.toString().padStart(3, '0')

        return """$hourPrime $hourDisplay:$minuteStr'$secondStr"$milliSecondStr"""
    }

    // 1000の補数を求める
    fun getMsComplement(): Long  {
        return 1000 - milliSecond
    }
}