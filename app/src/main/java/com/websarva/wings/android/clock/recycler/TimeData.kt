package com.websarva.wings.android.clock.recycler

data class TimeData(val key: String, val data: String)
