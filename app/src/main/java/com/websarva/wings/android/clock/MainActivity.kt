package com.websarva.wings.android.clock

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.websarva.wings.android.clock.recycler.RecyclerListAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    // default: true, 24h
    object MainObject {
        var fullTime = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* ツールバー設定するやつ 収納するために無駄にletに放り込む */
        let{
            val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
            toolbar.setTitle(R.string.toolbar_title)
            toolbar.setTitleTextColor(Color.WHITE)
            toolbar.setSubtitle(R.string.toolbar_subtitle)
            toolbar.setSubtitleTextColor(Color.LTGRAY)
            setSupportActionBar(toolbar)
        }


        val tvData = findViewById<TextView>(R.id.tvData)

        // TextViewを書き換える
        fun overwrite(getTime: GetTime){
            // rewrite TextView.text
            Log.i("MyClock.overwrite", "call getTime Instance")
            tvData.text = getTime.storeTimeInstance(MainObject.fullTime)
        }

        // リサイクラに入れるリストをインスタンス化する
        val listManager = ListManager()
        // リサイクラを生成,実装し、アダプタを返す
        val adapter = recyclerImplementer(listManager)

        // 時間を取得するクラスをインスタンス化
        var getTime = GetTime()


        class CoroutineCls : ViewModel() {
            fun timeWriteForwarder() {
                // コルーチンを開始
                viewModelScope.launch {
                    do {
                    // インスタンスの取得
                    getTime = GetTime()
                    // 表示の書き換え
                    overwrite(getTime)
                    // 現在時刻が *.000秒になるまで待つ
                    delay(getTime.getMsComplement())
                } while (true) }
            }
            // onCleared()でjobをキャンセル
            // ViewModelが死ぬタイミングで全てのコルーチンはキャンセルしておく
            @ExperimentalCoroutinesApi
            override fun onCleared() {
                super.onCleared()
                viewModelScope.cancel()
            }
        }
        // コルーチンを呼ぶやつ
        Log.i("MyClock.onCreate", "instance Coroutine")
        val sampleCls = CoroutineCls()
        Log.i("MyClock.onCreate", "call coroutine")
        sampleCls.timeWriteForwarder()

        // TextViewタップ時の処理
        tvData.setOnClickListener {
            MainObject.fullTime = !MainObject.fullTime
            Log.i("MyClock.setOnClickListener", "tvData listener")
            overwrite(getTime)
        }

        val fabBtn = findViewById<FloatingActionButton>(R.id.fabAdd)
        // FABボタンの処理
        fabBtn.setOnClickListener{
            Log.i("MyClock.FAB-Clicked", "store Data")
            val time: String = getTime.storeTimeInstance(MainObject.fullTime)
            listManager.addList(time)
            // リサイクラを更新
            adapter.notifyDataSetChanged()
        }
    }

    // リサイクラを生成,実装し、アダプタを返す
    fun recyclerImplementer(listManager: ListManager): RecyclerListAdapter {
        // リサイクラを準備
        val recyclerView = findViewById<RecyclerView>(R.id.lvMenu)
        val listMap = listManager.mutableList
        val adapter = RecyclerListAdapter(listMap, applicationContext)
        val layout = LinearLayoutManager(applicationContext)

        // リサイクラを実装
        recyclerView.apply {
            setHasFixedSize(true)
            recyclerView.layoutManager = layout
            recyclerView.adapter = adapter
        }

        // リサイクラービューは 同じものを使いまわすため adapter を返す
        return adapter
    }

    class ListManager{
        val mutableList: MutableList<MutableMap<String, *>> = mutableListOf()  // declare with initializer
        private var i: Int = 0
        fun addList(arg: String) {
            Log.i("MyClock.ListManager", "Adding item(${i},${arg})")
            val list = mutableMapOf("id" to i, "time" to arg)
            mutableList.add(list)
            i++
        }
    }


    /*
    private inner class ItemClickListener: View.OnClickListener {
        override fun onClick(view: View) {
            val tvWrappedTime = view.findViewById<TextView>(R.id.tvWrappedTime)
            val wrappedTime = tvWrappedTime.text.toString()
            val msg = "ID: $wrappedTime"
        }
    }
     */
}
// Log.i("MyClock.", "")

/**
 * TODO: ROOMデータベース実装
 * TODO: DBへの追加、削除機能の実装
 */