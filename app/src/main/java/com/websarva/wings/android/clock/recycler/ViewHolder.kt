package com.websarva.wings.android.clock.recycler

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.websarva.wings.android.clock.R

class RecyclerListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var tvWrappedTime : TextView = itemView.findViewById(R.id.tvWrappedTime)
}