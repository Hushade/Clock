package com.websarva.wings.android.clock.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.websarva.wings.android.clock.R

class RecyclerListAdapter(private val _listMap: MutableList<MutableMap<String, *>>, private val context: Context) : RecyclerView.Adapter<RecyclerListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerListViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.row, parent, false)
        //view.setOnClickListener(ItemClickListener())
        return RecyclerListViewHolder(view)
    }
    override fun onBindViewHolder(holder: RecyclerListViewHolder, position: Int) {
        val item = _listMap[position]
        val wrappedTime= "    " + item["time"] as String
        holder.tvWrappedTime.text = wrappedTime
    }

    override fun getItemCount(): Int {
        return _listMap.size
    }
}

